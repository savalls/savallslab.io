---
title: "Buñuelos de naranja"
image: 
  path: /assets/images/recipes/bunyuelos_naranja.jpg
  thumbnail: /assets/images/recipes/bunyuelos_naranja.jpg
  caption: "Photo from [pixabay](https://www.pixabay.com)"
---


Según la wikipedia, el [buñuelo](https://es.wikipedia.org/wiki/Bu%C3%B1uelo) es una masa de harina frita de origen español, que puede llevar un relleno (generalmente dulce) y que debido a su sencillez y rico sabor, ha traspasado fronteras.  Son un dulce tradicional en un gran número de fiestas tradicionales, como las fallas en Valencia, y resultan deliciosos a cualquier hora del día, especialmente en meriendas con un buen vaso de chocolate.

## Ingredientes

* 3 huevos.
* 1 yogurt.
* 200grs de harina.
* 1 naranja grande.
* 1 cucharada de aceite de oliva.
* 1 sobre de levadura en polvo.
* Aceite dee oliva para freir.
* Azúcar para rebozar.
* Sal.

## Elaboración

* Lavar cuidadosamente la naranja con un cepillo.  Cortarla a trocitos y triturarla.
* En un bol, añadimos la naranja triturada, el yogurt, los huevos y el aceite.  Lo batimos todo.
* Mezclar la harina, la levadura y la sal.  Añadir al bol con los demás ingredientes y mezclamos bien.
* Calentamos abundante aceite en una sartén.
* Con la ayuda de una cuchara, vamos haciendo bolas más o menos uniformes y se depositan en el aceite hirviendo.  Cuidado porque los buñuelos se doran rápidamente.
* Se apartan a una fuente con papel absorvente para eliminar los excesos de aceite, se espolvorean con azúcar y se sirven calientes.
