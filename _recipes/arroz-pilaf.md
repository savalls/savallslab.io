---
title: "Arroz Pilaf"
image: 
  path: /assets/images/recipes/arroz_pilaf.jpg
  thumbnail: /assets/images/recipes/arroz_pilaf.jpg
  #caption: "Photo from [Pexels](https://www.pexels.com/es-es/foto/comida-plato-cena-almuerzo-5678760/)"
---

Según [Wikipedia](https://www.wikipedia.org/), el arroz es originario de Asia, pero es justo en la época del califato abisí (S. VIII - XVI dc) que los métodos de cocción del arroz pilaf se extendieron desde España hacia Afganistán, y por extensión al resto de territorios.  está compuesto básicamente de verduras junto al arroz, por lo que podemos decir que se trata de una de las recetas más extendidas y universales.

Su forma de presentación es más cercana a una guarnición de un plato principal, que como plato principal en sí mismo.  Por su base más extendida en la cocina asiática, es normal cocinar la clase de arroz largo (basmati), aunque siempre podremos utilizar la que prefiramos por su disponibilidad.


## Ingredientes

* Aceite.
* Arroz.
* Ajo.
* Cebolla.
* Fondo Blanco.
* Puerro.
* Sal.
* Zanahoria.

## Elaboración

* Se lava la verdura y se corta en brunoise.
* Sofreir la verdura, empezando por la más dura, que requiere más tiempo, y terminando por la más blanda.
* Cuando está la verdura sofrita, se añade el arroz y se saltea.
* Se añade el fondo blanco.
* Se rectifica de sal y se deja cocer hasta que el arroz esté hecho. (18' aprox)
