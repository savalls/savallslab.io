---
title: "Muslo de pollo Albufera"
image: 
  #path: /assets/images/recipes/sopa_marisco.jpg
  #thumbnail: /assets/images/recipes/sopa_marisco.jpg
  #caption: "Photo from [pixabay](https://www.pixabay.com)"
---
## Ingredientes

* Muslos de pollo.
* Brunoise de verduras (zanahoria, cebolla, ajo, puerro, apio, etc...).
* Fondo blanco, (beloute).
* Sal.
* Pimienta blanca.
* Harina.

## Elaboración

* Caer a blanco la bronoisse de verdas.
* Salpimentar el pollo y pochar, adicionandolo a las verduras.
* Cubrir el pollo con el fondo blanco, y dejar cocer hasta que esté tierno.
* Una vez tierno, sacar y proceder a elaborar el _beoluté_ (mantequilla, harina y el fondo blanco).
* Dejar cocer durante unos minutos.

## Observaciones

* De guarnición, [arroz pilaf](https://s4v4lls.gitlab.io/recipes/arroz-pilaf/) y patatas naturales.
