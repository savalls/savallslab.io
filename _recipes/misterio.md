---
title: "Misterio"
image: 
  #path: /assets/images/recipes/chocolate-chip-cookies-lg.jpg
  #thumbnail: /assets/images/recipes/chocolate-chip-cookies-400x200.jpg
  #caption: "Photo from [Pexels](https://www.pexels.com)"
---

Este plato es típico de la gastronomía alicantina, combinando productos y adaptando diferentes recetas de la zona tan dispares como una fideuá con longaniza, champiñones y costillas de cerdo, o un arroz con costra.



## Ingredientes

 *  Longaniza.
 *  Costilla de cerdo.
 *  Champiñones cuarteados.
 *  Fideo fino (cabello de ángel)
 *  Caldo de cocido.
 *  Tomate triturado.
 *  Aceite, sal, colorante alimentario, ajo.
 *  Huevos escalfados (1/comensal)
 *  Pimientos.

## Elaboración

 1.  Freir las costillas y longanizas.
 2.  Añadir los champiñones cuarteados, los ajos (y los pimientos si se desea)
 3.  Cuando está sofrito, añadir los fideos y el colorante (tiempo == 5min)
 4.  En otro recipiente, calentas el caldo del cocido hasta su ebullición
 5.  Añadir el caldo al sofrito, y cuando comience a hervir, se añade un huevo escalfado por persona.



