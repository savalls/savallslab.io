---
title: "Lacón con grelos"
image: 
  path: /assets/images/recipes/lacon_grelos.jpg
  thumbnail: /assets/images/recipes/lacon_grelos.jpg
  caption: "Photo from [flickr](https://www.flickr.com/photos/luisazpiazu/5464293719/in/photolist-9jRW9F-bwVE42-aaGTyf-aaDYYM-bwVF6D-bwVEL6-bwVEAR-bwVEtB-bwVEiR-bwVFwi-bwVEV4-bwVFPi-bwVFop-bwVFdV-bwVFEn-bwVG2H-7BmkQY-kQ8E68-e4ChxH-e4HTsU-2mnzJBN-e4Chdx-e4Ch6F-7QeGBD-CRywNz-dMJzkX-9riAM5-7Bmmvs-buHtQ6-qqek9w-q8RGbP-2mnuMsU-qo1ZEw-2mnw48m-e4ChQM-ptiNif-2mnyCG2-5RaRan-ptiACS-ptxhne-qqeDQj-qqepJy-ptx2n4-DCwxs3-q8Rpj2-qo219s-ptxh6x-ptiFCb-qqeop9-ptiLkY)"
---
El lacón con grelos en sus inicios se consumía durante la celebración del Carnaval, al ser la mejor época para los grelos, actualmente se puede encontrar en los mejores restaurantes de Galicia y del resto de España. Recuerda en algunos aspectos la cocina germana (codillo de cerdo).

## Ingredientes #### (4 personas)

* 400gr de lacón (brazuelo de cerdo fresco)
* 400gr de grelos (nabos con su tallo)
* 320gr de judías blancas.
* 2 cucharadas de manteca de cerdo, o aceite.
* Agua.
* Sal.

## Elaboración

1. En un recipiente poner las judías blancas, previamente remojadas y cubriéndolas con agua.
2. Cuando estén casi cocidas añadir la manteca o el aceite;  una vez cocidas, incorporar el lacón troceado, los grelos (bien lavados y troceados)
3. Servir cuando esté bien hervido y blando, al punto de sal.Agregar el calabacín.  rehogar y añadir 3l de agua.


#### Observaciones:  No debe quedar muy espeso.
