---
title: "Pudin Diplomático"
image: 
  path: /assets/images/recipes/pudin-diplomatico.jpg
  thumbnail: /assets/images/recipes/pudin-diplomatico.jpg
  caption: "Photo from [solpostres](https://www.solopostres.com)"
---
El budín diplomático es típico de la cocina cubana y se caracteriza por llevar pan, frutas frescas y secas a gusto.
Existen cientos de variedades de budines dulces éste que es preparado con diferentes ingredientes y cubierto con un caramelo que es digno de ser probado.



## Ingredientes

* 1 litro de leche aromatizada (vainilla, canela, corteza de limón)
* 200grs de azúcar.
* 7-8 huevos, según tamaño.
* 200grs de fruta o almíbar.
* Bizcocho o pan de molde.
* Molde rectangular acaramelado.



## Elaboración

* Poner a hervir la leche con el perfume.
* Caramelizar el molde, colocándolo sobre una placa con papel al baño maría.
* Batir ligeramente los huevos con el azúcar, adicionando la leche hirviendo, a través de un chino sobre la mezcla sin dejar de remover.
* Rellenar el molde con bizcocho troceado cubriendo hasta la mitad del molde, y colocando la fruta picada en el medio, en toda su longitud, y presionando ligeramente.
* Adicionar la crema de leche hirviendo y rellenar el molde en tres partes.
* Cocinar al baño maría en un horno moderado (150ºC), e iremos adicionando durante la cocción el resto de leche.



#### Observaciones

* El tiempo de cocción aproximado es de 1:30h.
