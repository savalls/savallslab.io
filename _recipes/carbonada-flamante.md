---
title: "Carbonada Flamante"
image: 
  path: /assets/images/recipes/carbonade_flamande.jpg
  thumbnail: /assets/images/recipes/carbonade_flamande.jpg
  caption: "Photo from [wikipedia](https://www.wikipedia.com)"
---
Este plato recibe el nombre porque es originario de Flandes ([_Carbonade Flamande_](https://es.wikipedia.org/wiki/Carbonade) es uno de los platos nacionales), y antiguamente se cocinaba sobre las brasas de la cocina en un recipiente tapado.  Aunque el plato es de ternera, la carbonada siempre va a llevar como ingredientes principales la cebolla y la cerveza.  No obstante, se puede sustituir la carne de ternera por la de cerdo.

## Ingredientes

* Escalopes de Ternera.
* Nuez moscada.
* Sal.
* Harina.
* Azucar moreno.
* Mantequilla.
* Vinagre de vino blanco.
* Cebolla cortada en juliana.
* Cerveza.
* Fondo de ternera.
* Pimienta negra molida.
* Caldo de carne.
* Hierbas aromáticas.

## Elaboración

* Sazonar los escalopes con sal y pimienta, y enharinados, los freímos con mantequilla y reservamos.
* En la misma mantequilla sofreímos la cebolla, y cuando esté dorada añadimos una cucharadita de harina, sin parar de remover para que dore también.
* Añadimos la carne que hemos reservado anteriormente, el vinagre, el azúcar y las hierbas aromáticas (las tendremos envueltas en un muselina para que no se pierdan).  Y removemos todo un poco.
* Adicionamos la cerveza y el fondo de ternera.  Tapamos y dejamos cocer todo durante 1h30min aprox.
* Retirar las hierbas y servir.
