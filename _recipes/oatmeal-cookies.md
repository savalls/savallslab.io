---
title: "Galletas de avena"
image: 
  path: /assets/images/recipes/oatmeal-cookies-lg.jpg
  thumbnail: /assets/images/recipes/oatmeal-cookies-400x200.jpg
  caption: "Photo from [Pexels](https://www.pexels.com)"
---

Las galletas de avena son un favorito proverbial tanto para niños como para adultos. Esta galleta crujiente y masticable está cargada de avena, frutas secas y nueces picadas.

## Ingredientes

* 1 taza de mantequilla, ablandada 1 taza de azúcar blanca
* 1 taza de azúcar morena envasada
* 2 huevos
* 1 cucharadita de extracto de vainilla
* 2 tazas de harina para todo uso
* 1 cucharadita de bicarbonato de sodio
* 1 cucharadita de sal
* 1 1/2 cucharaditas de canela molida
* 3 tazas de avena de cocción rápida

## Instrucciones

1. En un tazón mediano, mezcle la mantequilla, el azúcar blanco y el azúcar moreno. Batir los huevos uno a la vez, luego agregar la vainilla. Combine la harina, el bicarbonato de sodio, la sal y la canela; revuelva en la mezcla cremosa. Mezclar con avena. Cubra y enfríe la masa durante al menos una hora.
2. Precaliente el horno a 190ºC. Engrase las bandejas para hornear galletas. Enrolle la masa en bolas del tamaño de una nuez y colóquelas a 2 pulgadas de distancia en bandejas para hornear galletas. Aplane cada galleta con un tenedor grande sumergido en azúcar.
3. Hornee de 8 a 10 minutos en horno precalentado. Deje que las galletas se enfríen en una bandeja para hornear durante 5 minutos antes de transferirlas a una rejilla para que se enfríen por completo.
