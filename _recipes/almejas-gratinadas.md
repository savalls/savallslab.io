---
title: "Almejas gratinadas"
image: 
  path: /assets/images/recipes/almejas_gratinadas.jpg
  thumbnail: /assets/images/recipes/almejas_gratinadas.jpg
  #caption: "Photo from [Pexels](https://www.pexels.com/es-es/foto/comida-plato-cena-almuerzo-5678760/)"
---





## Ingredientes

* Almejas
* Pescado variado.
* Cebolla, ajo, perjeil.
* Veoluté (harina, mantequilla, fumet).
* Queso rallado.
* Sal y pimienta.

## Elaboración

1. Abrir las almejas, cortarlas junto al pescado variado, ajo y cebolla. Rehogar.
2. Elaborar la veoluté.
3. Mezclar la masa de relleno con la veoluté y rellenar la almeja.
4. Cubrir la almeja con queso rallado y gratinar.gar con la provenzal.
