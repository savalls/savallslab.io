---
title: "Escalopes de cerdo al curry"
image: 
  path: /assets/images/recipes/escalopes_cerdo_curry.jpg
  thumbnail: /assets/images/recipes/escalopes_cerdo_curry.jpg
  caption: "Photo from [Flickr](https://www.flicker.com)"
---
## Ingredientes

* Escalopes de cerdo.
* 1 Cebolla grande.
* 1 Manzana grande.
* 30grs de mantequilla.
* 200 ml crema de leche.
* Fondo Blanco.
* Tomillo.
* curry.
* Sal.
* Zanahoria.

## Elaboración

**Salsa Curry:**

* Se fondea la cebolla y la manzana, que previemente habremos cortado en juliana, en mantequilla.
* Adicionar la crema de leche y dejar reducir.

* Echar la hoja de laurel, tomillo, fondo blanco, y el curry en polvo.
* Se cocina al punto, rectificando de sal si es necesario, se pasa por un chino y se reserva.


**Escalopes de cerdo:**

* Salpimentar la carne, se enharina y se fríe en aceite.


## Observaciones:

* Guarnición para acompañar puede ser cualquier verdura o patatas risoladas.
