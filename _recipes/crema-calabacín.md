---
title: "Crema de calabacín"
image: 
  path: /assets/images/recipes/crema_calabacin.png
  thumbnail: /assets/images/recipes/crema_calabacin.png
  caption: "Photo from [pixabay](https://www.pixabay.com)"
---


La crema de calabacín (zapallo italiano) es una receta muy ligera, nutritiva y, además, deliciosa. Entre sus propiedades, es una receta principalmente hidratante, depurativa y diurética, que limpia y lubrica nuestro tracto intestinal. Proporciona muchos electrolitos (potasio y sodio), fibra, minerales como el magnesio, vitaminas B, y muchos antioxidantes como la quercetina de la cebolla o la clorofila.

Es una receta ideal en toda dieta saludable, para todas las edades y etapas de la vida.


## Ingredientes (10 personas)

* 1kg de calabacines.
* 2 cebollas.
* 3 patatas.
* 6 quesitoss.
* Dados de pan tostado.
* Sal y pimienta.

## Elaboración

1. Sofreir la cebolla cortada en trozos.
2. Agregar el calabacín.  rehogar y añadir 3l de agua.
3. Cuando alcance el punto de ebullición, añadir las patatas cortadas a trozos.
4. Después de haber hervido lo suficiente, añadiremos los quesitoss, salpimentaremos y pasaremos por la batidora.
5. Servir acompañado de costrones fritos de pan.
