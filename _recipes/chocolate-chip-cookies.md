---
title: "Galletas con chispas de Chocolate"
image: 
  path: /assets/images/recipes/chocolate-chip-cookies-lg.jpg
  thumbnail: /assets/images/recipes/chocolate-chip-cookies-400x200.jpg
  caption: "Photo from [Pexels](https://www.pexels.com)"
---

Las galletas con pepitas de chocolate se originaron en los Estados Unidos y presentan trocitos de chocolate en forma de pepitas distribuidas en su interior, como ingrediente distintivo.

La receta tradicional combina una masa compuesta por mantequilla y azúcar moreno y blanco con pepitas de chocolate semidulce. Las variaciones incluyen recetas con otros tipos de chocolate, así como ingredientes adicionales como nueces o avena.

Esta receta es para un volumen estimado de 4 docenas de galletas.

## Ingredientes

* 2 1/4 tazas de harina para todo uso
* 1 cucharadita de bicarbonato de sodio
* 1/2 cucharadita de sal
* 1 taza de mantequilla, ablandada y cortada en trozos
* 1 taza de azúcar
* 1 taza de azúcar morena clara, envasada
* 2 cucharaditas de extracto de vainilla
* 2 huevos grandes
* 2 tazas de chispas de chocolate semidulce
* 1/2 cucharadita de nuez moscada (opcional)
* 1 taza de pecanas o nueces picadas (opcional)

## Instrucciones

1. Precaliente el horno a 180º C.
2. En un tazón mediano, mezcle la harina con bicarbonato de sodio, nuez moscada y sal.
3. En un tazón grande, bata la mantequilla con el azúcar y el azúcar moreno hasta que quede cremoso y ligero. Agregue la vainilla y los huevos, uno a la vez, y mezcle hasta que se incorporen.
4. Agregue gradualmente la mezcla seca a la mezcla húmeda de mantequilla y azúcar, mezclando con una espátula hasta que se mezclen. Agregue las chispas de chocolate y las nueces hasta que se mezclen.
5. Coloque grumos del tamaño de una cucharada en bandejas para hornear galletas sin engrasar. Hornee durante 8-12 minutos, o hasta que estén doradas. Deje que se enfríen en la bandeja durante uno o tres minutos, luego transfiera las galletas a una rejilla para que terminen de enfriarse.
