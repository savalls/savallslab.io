---
title: "Crema pastelera"
image: 
  path: /assets/images/recipes/crema_pastelera.png
  thumbnail: /assets/images/recipes/crema_pastelera.png
  caption: "Photo from [flickr](https://www.flikr.com)"
---


La crema pastelera es la reina de las cremas de la cocina tradicional y la base de muchas cremas tradicionales como la crema catalana, las natillas o la custard británica.
 Sus componentes básicos son leche, huevos, azúcar y harina de trigo (harina común). Algunas personas reemplazan la harina con fécula de maíz (maicena).  Se aromatiza con vainilla, canela, y limón o naranja en esencia o su ralladura.

Su origen es bastante incierto, algunos investigadores la ubican en Francia como sub producto de la Crème Bruleè y otros en Italia como un derivado del Roux Blanco. Lo cierto es que su uso está bastante extendido en todas las cocinas. 

## Ingredientes

* 1 litro de leche aromatizada (leche, canela, limón)
* 250grs de azúcar.
* 3 yemas de huevo.
* 90grs de harina.
* 20grs de maicena.
* 40grs de mantequilla

## Elaboración

 *  Mezclar el azúcar, harina y maicena, a continuación los huevos.
 *  Mezclar con la leche caliente, y por último la mantequilla.

#### Observaciones
*  La leche previamente caliente, ayuda a la mejor elaboración de la crema.


