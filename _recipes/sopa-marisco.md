---
title: "Sopa de Marisco"
image: 
  path: /assets/images/recipes/sopa_marisco.jpg
  thumbnail: /assets/images/recipes/sopa_marisco.jpg
  caption: "Photo from [pixabay](https://www.pixabay.com)"
---


  Comer pescado aporta muchos beneficios a nuestro organismmo, y nada mejor que una sopa para depurarlo.
  Destacar que supone una fuente de vitaminas B12, A, D, hierro, fósforo, Magnesio, yodo, calcio,  minerales y ácidos grasos, ayuda a controlar el colesterol, es bajo en calorías, etc.. 

## Ingredientes

* 6 Cigalas.
* 6 Gambas.
* 12 Rodajas de calamar.
* 100grs de rape.
* 12-18 Mejillones.
* 100grs de emperador.
* 1 cebolla mediana.
* 2 ajos, perejil, laurel, sal, aceite.
* 1 cucharada de pimentón
* 1 cucharada de harina.
* 2 cucharadas de tomate.
* 1 vasito de cognac o brandi.
* Fumet de pescado.

## Elaboración

* Sofreir todo el pescado y el marisco.  Reservamos.
* En el mismo aceite, caer a blanco la cebolla con el perejil y el ajo.  
* Adicionar el pimentón, la harina y el tomate.
* Removemos y añadimos el fumet de pescado junto al pescado y marisco que tenemos apartado.
* Se deja cocer unos minutos y a la hora de servir, rociar con pernof (Ricart).
