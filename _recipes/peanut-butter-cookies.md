---
title: "Galletas de mantequilla de cacahuete"
image: 
  path: /assets/images/peanut-butter-cookies-lg.jpg
  thumbnail: /assets/images/peanut-butter-cookies-400x200.jpg
  caption: "Photo from [Pexels](https://www.pexels.com)"
---

Una galleta de mantequilla de cacahuete es un tipo de galleta que se distingue por tener mantequilla de cacahuete como ingrediente principal. La galleta generalmente se originó en los Estados Unidos, su desarrollo se remonta a la década de 1910.

## Ingredientes

* 1 taza de mantequilla sin sal
* 1 taza de mantequilla de cacahuete crocante
* 1 taza de azúcar blanca
* 1 taza de azúcar morena envasada
* 2 huevos de clase 2
* 1/2 tazas de harina para todo uso
* 1 cucharadita de polvo de hornear
* 1/2 cucharadita de sal
* 1 1/2 cucharaditas de bicarbonato de sodio

## Instrucciones

1. Batir la mantequilla, la mantequilla de maní y los azúcares en un tazón; batir los huevos.   
2.  En un recipiente aparte, tamice la harina, el polvo de hornear, el bicarbonato de sodio y la sal; agregue a la mezcla de mantequilla. Ponga la masa en el refrigerador durante 1 hora.   
3.  Enrolle la masa en bolas de 1 pulgada y colóquelas en bandejas para hornear. Aplane cada bola con un tenedor, formando un patrón entrecruzado. Hornee en un horno precalentado a 190ºC durante unos 10 minutos o hasta que las galletas comiencen a dorarse.
