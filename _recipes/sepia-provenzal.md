---
title: "Sepia a la provenzal"
image: 
  #path: /assets/images/recipes/chocolate-chip-cookies-lg.jpg
  #thumbnail: /assets/images/recipes/chocolate-chip-cookies-400x200.jpg
  #caption: "Photo from [Pexels](https://www.pexels.com)"
---


[A la provenzal](https://diccionariodegastronomia.com/word/a-la-provenzal/)

Técnica de preparación que consiste en incorporar al alimento una mezcla
de pan rallado, perejil, ajo picado, aceite de oliva o mantequilla y
varias hierbas y especias típicas de la Provenza, de donde viene su
nombre, entre las que se encuentran el tomillo, el romero, la albahaca,
el orégano, el estragón y la lavanda. Se elaboran con esta técnica
pescados, carnes e incluso mariscos, a los que dota de un intenso sabor y
aroma.


## Ingredientes

* Cebolla cortada en juliana.
* Provenzal (perejil, pimentón, ajo picado y pan rayado).
* Vino blanco.
* Fumet o caldo.



## Elaboración

1. Caer a blanco la cebolla.
2. Adicionar la sepia, previamente salteada, vino blanco y el fumet de pescado.
3. Dejar cocer y cuando la sepia está tierna, ligar con la provenzal.