---
title: "Publicación:  Fecha Modificada"
categories:
  - Formato de Publicaciones
tags:
  - Formato de Publicaciones
  - Legibilidad
  - standard
last_modified_at: 2017-03-09T13:01:27-05:00
---

Esta publicación ha sido actualizada y debe mostrar una fecha modificada si la última modificada se usa en el diseño.

Plugins como [**jekyll-sitemap**](https://github.com/jekyll/jekyll-feed) usa este campo para agregar un `<lastmod>` tag a tu `sitemap.xml`.