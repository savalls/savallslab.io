---
title: "Caso Extremo: Listas anidadas y mixtas."
categories:
  - Caso extremo
tags:
  - contenidos
  - css
  - caso extremo
  - listas
  - margen
last_modified_at: 2021-01-01T14:45:52-05:00
---
Las listas anidadas y mixtas suponen una problemática interesante. Siempre hay que asegurarse que las diferentes variables, formadas por listas dentro de otras listas, no rompen el orden de numeración de las listas ordenadas, y que los estilos de lista sean lo suficientemente profundos, mediante indentaciones.

## Ordenado -- Desordenado -- Ordenado

1. item ordenado
2. item ordenado

* **desordenado**
* **desordenado**
  1. item ordenado
  2. item ordenado

3. item ordenado
4. item ordenado

## Ordenado -- Desordenado -- Desordenado

1. item ordenado
2. item ordenado

* **desordenado**
* **desordenado**
  * item desordenado
  * item desordenado

3. item ordenado
4. item ordenado

## Desordenado -- Ordenado -- Desordenado

* item desordenado
* item desordenado

  1. ordenado
  2. ordenado

  * item desordenado
  * item desordenado
* item desordenado
* item desordenado

## Desordenado -- Desordenado -- Ordenado

* item desordenado
* item desordenado
  * desordenado
  * desordenado
    1. **item ordenado**
    2. **item ordenado**
* item desordenado
* item desordenado
