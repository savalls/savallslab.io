---
categories:
  - Caso Extremo
tags:
  - caso extremo
  - diseño
  - título
---

Esta publicación no tiene título especificado en el archivo YAML frontal.  jekyll autogenerará un título a partir del archivo.

Por ejemplo 2019-09-05-caso-extremo-sin-titulo-yaml.md, se convierte en **Caso Extremo Sin Titulo**.