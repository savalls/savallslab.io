---
title: "Diseño: Imagen héroe externo"
image: 
  path: https://source.unsplash.com/random/1024x600
  thumbnail: https://source.unsplash.com/random/400x300
categories:
  - Diseño
tags:
  - contenido
  - imagen
  - diseño
last_modified_at: 2017-03-17T10:46:49-04:00
---

Esta publicación debe mostrar una imagen de heroe grande en la parte superior de una página.

Esta publicación prueba una imagen horizontal, usando el siguiente YAML Front Matter:

```yaml
image: https://source.unsplash.com/random/1024x600
```

Las imágenes del heroe también se pueden asignar así:

```yaml
image:
  path: https://source.unsplash.com/random/1024x600
```
