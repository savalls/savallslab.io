---
title: "Publicación: La cita"
categories:
  - Formatos de Publicación
tags:
  - Formatos de Publicación
  - Cita
---

> Solo una cosa es imposible para Dios: encontrar algún sentido a cualquier ley de derechos de autor en el planeta.
> 
> <cite><a href="http://www.brainyquote.com/quotes/quotes/m/marktwain163473.html">Mark Twain</a></cite>