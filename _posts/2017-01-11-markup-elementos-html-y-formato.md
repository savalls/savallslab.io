---
title: "Markup: Elementos HTML y Formato"
sub_title: "Elementos comunes"
categories:
  - Markup
elements:
  - contenido
  - css
  - formato
  - html
  - markup
last_modified_at: 2017-03-09T10:55:59-05:00
---

Una variedad de elementos HTML comunes para demostrar la hoja de estilo del tema y verificar que se hayan diseñado correctamente.

# Encabezamiento uno

## Encabezamiento dos

### Encabezamiento tres

#### Encabezamiento cuatro

##### Encabezamiento cinco

###### Encabezamiento seis

## Bloque de comillas

Cita en bloque de una sola línea:

> Quédate con hambre. Mantente tonto.

Cita en bloque de varias líneas con una referencia de cita:

> La gente piensa que enfocarse significa decir sí a aquello en lo que tienes que concentrarte. Pero eso no es lo que significa en absoluto. Significa decir no a las otras cien buenas ideas que hay. Usted tiene que escoger con cuidado. De hecho, estoy tan orgulloso de las cosas que no hemos hecho como de las cosas que he hecho. La innovación es decir no a 1.000 cosas.

<cite>Steve Jobs</cite> --- Conferencia mundial de desarrolladores de Apple, 1997
{: .small}

## Tablas

| Empleado         | Salario |                                                              |
| --------         | ------ | ------------------------------------------------------------ |
| [John Doe](#)    | $1     | Porque eso es todo lo que Steve Jobs necesitaba para un salario.           |
| [Jane Doe](#)    | $100K  | Por todos los blogs que ella hace.                               |
| [Fred Bloggs](#) | $100M  | Las imágenes valen más que mil palabras, ¿verdad? Entonces Jane × 1,000. |
| [Jane Bloggs](#) | $100B  | ¿Con el pelo así? Basta de charla.                           |

| Encabezamiento1 | Encabezamiento2 | Encabezamiento3 |
|:--------|:-------:|--------:|
| celda1   | celda2   | celda3   |
| celda4   | celda5   | celda6   |
|-----------------------------|
| celda1   | celda2   | celda3   |
| celda4   | celda5   | celda6   |
|=============================|
| Pie1   | Pie2   | Pie3   |

## Listas de definición

Título de la lista de definiciones.
: Definición división de lista.

Puesta en marcha
: Una startup o startup es una empresa u organización temporal diseñada para buscar un modelo de negocio repetible y escalable.

#Hacer trabajo
: Acuñado por Rob Dyrdek y su guardaespaldas personal Christopher "Big Black" Boykins, "Do Work" funciona como auto motivador, para motivar a tus amigos.

Hazlo en vivo
: Dejaré que Bill O'Reilly [explique](https://www.youtube.com/watch?v=O_HyZ5aW76c "We'll Do It Live") esto.

## Listas desordenadas (anidadas)

    * Elemento de la lista uno
       * Elemento de la lista uno
           * Elemento de la lista uno
           * Elemento de la lista dos
           * Elemento de la lista tres
           * Elemento de la lista cuatro
       * Elemento de la lista dos
       * Elemento de la lista tres
       * Elemento de la lista cuatro
   * Elemento de la lista dos
   * Elemento de la lista tres
   * Elemento de la lista cuatro

## Lista ordenada (anidada)

1. Elemento de la lista uno
       1. Elemento de la lista uno
           1. Elemento de la lista uno
           2. Elemento de la lista dos
           3. Enumere el elemento tres
           4. Elemento de la lista cuatro
       2. Elemento de la lista dos
       3. Enumere el elemento tres
       4. Elemento de la lista cuatro
   2. Enumere el elemento dos
   3. Enumere el elemento tres
   4. Elemento de la lista cuatro

## Elemento de dirección

<address>
  1 Infinite Loop<br /> Cupertino, CA 95014<br /> United States
</address>

## Elemento de anclaje (también conocido como enlace)

Esto es un ejemplo de [enlace](http://apple.com "Apple").

## elemento de abreviatura

La abreviatura CSS significa "hojas de estilo en cascada".

*[CSS]: Hojas de Estilo en Cascada

## Citar Elemento

"El código es poesía." ---<cite>Automático</cite>

## Elemento de código

Aprenderás más adelante en estas pruebas que `word-wrap: break-word;` será tu mejor amigo.

## Elemento de huelga

Este elemento te permitirá <strike>texto tachado</strike>.

## Elemento de énfasis

El elemento de énfasis debe poner el texto en cursiva.

## Insertar elemento

Este elemento debe indicar texto <ins>insertado</ins>.

## Elemento del teclado

Este elemento apenas conocido emula <kbd>texto de teclado</kbd>, que generalmente tiene el estilo del elemento `<code>`.

## Elemento preformateado

Este elemento aplica estilos a grandes bloques de código.

<pre>
.post-title {
	margin: 0 0 5px;
	font-weight: bold;
	font-size: 38px;
	line-height: 1.2;
	y aquí hay una línea de un texto muy, muy, muy, muy largo, solo para ver cómo lo maneja el elemento PRE y para descubrir cómo se desborda;
}
</pre>

## Elemento entrecomillado

<q>Desarrolladores, desarrolladores, desarrolladores&#8230;</q> &#8211;Steve Ballmer

## Elemento fuerte

Este elemento muestra **texto en negrita**.

## Elemento subíndice

Poniendo nuestro estilo científico con H<sub>2</sub>O, que debería empujar el "2" hacia abajo.

## Elemento de superíndice

Sigo con la ciencia y E = MC<sup>2</sup> de Isaac Newton, que debería elevar el 2.

## Elemento variable

Esto le permite denotar <var>variables</var>.