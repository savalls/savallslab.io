---
title: "Markup: Título *con* **Markdown**"
categories:
  - Markdown
tags:
  - css
  - html
  - título
last_modified_at: 2017-03-09T12:25:10-05:00
---

El uso de Markdown en el título no debería tener efectos adversos en el layout o la funcionalidad.

**`page.title` ejemplo:**

```yaml
title: "Markup: Título *con* **Markdown**""
```