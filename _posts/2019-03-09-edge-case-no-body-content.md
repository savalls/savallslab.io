---
title: "Caso Extremo: Cuerpo Sin Contenido"
excerpt: "Esta publicación no tiene cuerpo y la página de la publicación debería estar en blanco o vacía."
categories:
  - Caso Extremo
tags:
  - contenido
  - caso extremo
  - diseño
last_modified_at: 2019-03-09T14:23:48-05:00
---
