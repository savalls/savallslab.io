---
title: "Disposición: extracto (definido)"
excerpt: "Este es un extracto de publicación definido por el usuario. Debe mostrarse en lugar del extracto generado automáticamente o publicar contenido en las páginas de índice."
categories:
  - Diseño
  - Sin Categoria.
tags:
  - contenido
  - extracto
  - disposición
last_modified_at: 2017-03-09T12:43:31-05:00
---

Éste es el principio del contenido de la publicación.

Este párrafo debe estar ausente de una página índice donde se muestra `post.excerpt`.