---
title: "Publicación: Fecha Future"
date: 9999-12-31
categories:
  - Publicación
last_modified_at: 2017-03-09T12:45:25-05:00
---

Esta publicación está establecido en el futuro y está datada en  {{ page.date | date: "%c" }}. Sólo suele aparecer cuando jekyll construye tu proyecto con la bandera (flag) `--future`.

```bash
jekyll build --future
```