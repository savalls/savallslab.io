---
title: "Markup: Sintaxis Destacada"
excerpt: "Publicación que muestra las diversas formas en que se pueden destacar bloques de código con Jekyll. Algunas opciones incluyen Markdown estándar, GitHub Flavored Markdown y Jekyll's `{% destacado %}` tag."
last_modified_at: 2017-03-09T10:27:01-05:00
tags: 
  - código
  - sintaxis destacada
---

El destacado de sintaxis es una característica que muestra el código fuente, en diferentes colores y fuentes según la categoría de los términos. Esta función facilita la escritura en un lenguaje estructurado, como un lenguaje de programación o un lenguaje de marcado, ya que tanto las estructuras como los errores de sintaxis son visualmente distintos. El destacado no afecta al significado del texto en sí; está destinado únicamente a lectores humanos.[^1]

[^1]: <http://en.wikipedia.org/wiki/Syntax_highlighting>

## GFM Bloques de Codigo

GitHub Flavored Markdown [bloques de código delimitados](https://help.github.com/articles/creating-and-highlighting-code-blocks/) son compatibles de forma predeterminada con Jekyll. Es posible que deba actualizar su archivo `_config.yml` para habilitarlos si está utilizando una versión anterior.

```yaml
kramdown:
  input: GFM
```

Aquí hay un ejemplo de un fragmento de código CSS escrito en GFM:

```css
#contenedor {
  float: left;
  margin: 0 -240px 0 0;
  width: 100%;
}
```

Otro fragmento de código más para fines de demostración:

```ruby
modulo Jekyll
  class TagIndex < Page
    def initialize(site, base, dir, tag)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tag_index.html')
      self.data['tag'] = tag
      tag_title_prefix = site.config['tag_title_prefix'] || 'Tagged: '
      tag_title_suffix = site.config['tag_title_suffix'] || '&#8211;'
      self.data['title'] = "#{tag_title_prefix}#{tag}"
      self.data['description'] = "An archive of posts tagged #{tag}."
    end
  end
end
```

## Tag Jekyll Liquid destacado

Jekyll también tiene soporte incorporado para el resaltado de sintaxis de fragmentos de código usando Rouge o Pygments, usando una etiqueta Liquid dedicada de la siguiente manera:

```liquid
{% raw %}{% highlight scss %}
.highlight {
  margin: 0;
  padding: 1em;
  font-family: $monospace;
  font-size: $type-size-7;
  line-height: 1.8;
}
{% endhighlight %}{% endraw %}
```

Y el resultado mostrará algo así:

{% highlight scss %}
.highlight {
  margin: 0;
  padding: 1em;
  font-family: $monospace;
  font-size: $type-size-7;
  line-height: 1.8;
}
{% endhighlight %}

Aquí hay un ejemplo de código snippet usando el tag Liquid y `linenos` activado.

{% highlight html linenos %}
{% raw %}<nav class="pagination" role="navigation">
  {% if page.previous %}
    <a href="{{ site.url }}{{ page.previous.url }}" class="btn" title="{{ page.previous.title }}">Articulo anterior</a>
  {% endif %}
  {% if page.next %}
    <a href="{{ site.url }}{{ page.next.url }}" class="btn" title="{{ page.next.title }}">Next article</a>
  {% endif %}
</nav><!-- /.pagination -->{% endraw %}
{% endhighlight %}

## Boques de Código en Listas

La sangría importa. Asegúrese de que la sangría del bloque de código se alinee con el primer carácter que no sea un espacio después del marcador del elemento de la lista (por ejemplo, `1.`). Por lo general, esto significará sangrar 3 espacios en lugar de 4.

1. Haz el paso 1.
2. Ahora haz esto:
   
   ```ruby
   def print_hi(name)
     puts "Hi, #{name}"
   end
   print_hi('Tom')
   #=> prints 'Hi, Tom' to STDOUT.
   ```
        
3. Ahora puedes hacer esto.

## Incrustación de GitHub Gist

Las incrustaciones de GitHub Gist también se pueden usar:

```html
<script src="https://gist.github.com/mmistakes/77c68fbb07731a456805a7b473f47841.js"></script>
```

El resultado de su salida es:

<script src="https://gist.github.com/mmistakes/77c68fbb07731a456805a7b473f47841.js"></script>
