---
title: "Antidesestablecimentarismo"
categories:
  - Caso Extremo
tags:
  - contenido
  - css
  - caso extremo
  - html
  - diseño
  - titulo
last_modified_at: 2019-10-06T14:10:02-05:00
---

El título de esta publicación contiene una palabra larga que puede desbordar, potencialmente, el area de contenido.

Algunos conceptos a revisar:

  *  Texto continuo en el título no debe tener efectos adveros en el diseño o laa funcionalidad.
  * Comprobar el título de la pestaña / ventana del navegador.
  
  

La siguiente propiedad de CSS le ayudará a admitir texto sin interrupciones.

```css
word-wrap: break-word;
```