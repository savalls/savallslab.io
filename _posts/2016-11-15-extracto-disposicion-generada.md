---
title: "Disposición: Extracto (Generado con el Tag de Separación)"
excerpt_separator: "<!--more-->"
categories:
  - Disposición
  - Sin Categoria
tags:
  - contenido
  - extracto
  - disposición
last_modified_at: 2017-03-09T12:32:16-05:00
---

Este es el contenido de la publicación. Las páginas de índice de archivo deben mostrar un [extracto autogenerado](https://jekyllrb.com/docs/posts/#post-excerpts) de todo el contenido que precede a `excerpt_separator`, como se define en YAML Front Matter o globalmente en `_config.yml`.

Asegúrese de probar el formato del extracto generado automáticamente, para asegurarse de que no cree ningún problema de diseño.

<!--more-->

Lorem ipsum dolor sit amet, dicant nusquam corpora in usu, laudem putent fuisset ut eam. Justo accusam definitionem id cum, choro prodesset ex his. Noluisse constituto intellegebat ea mei. Timeam admodum omnesque pri ex, eos habemus suavitate aliquando cu. Dico nihil delectus quo cu. Ludus cetero cu eos, vidit invidunt dissentiet mea ne.

Usu delenit vocibus elaboraret ex. Scripta sapientem adversarium ei pri, pri ex solet democritum. Nam te porro impedit, ei doctus albucius cotidieque pri, ea mutat causae lucilius has. Pri omnis errem labore ut. An aperiam tibique est, mei te dolore veritus, nam nulla feugait ut. In vis labitur eripuit contentiones.