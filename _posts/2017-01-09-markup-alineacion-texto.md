---
title: "Markup: Alineación de Texto y Transformaciones."
categories:
  - Markup
tags:
  - alineación
  - contenido
  - css
  - markup
last_modified_at: 2017-03-09T12:17:03-05:00
---

Ejemplo de texto para demostrar clases de alineación y transformación.

Realignar fácilmente el texto con clases de alineación a través de HTML:

```
<p class="text-left">Texto alineado a la izquierda.</p>
<p class="text-center">Texto Centrado.</p>
<p class="text-right">Texto alineado a la derecha.</p>
<p class="text-justify">Texto justificado.</p>
<p class="text-nowrap">Texto sin ajuste (no wrap text).</p>
```

O mediante Kramdown and [listas de atributoss en linea](https://kramdown.gettalong.org/syntax.html#inline-attribute-lists):

```markdown
Texto alineado a la izquierda.
{: .text-left}

Texto centrado.
{: .text-center}

Texto alineado a la derecha.
{: .text-right}

Texto justificado.
{: .text-justify}

Texto sin ajuste (no wrap text).
{: .text-nowrap}
```

## Por defecto

Este es un párrafo. No debe tener ninguna alineación de ningún tipo. Debería fluir como lo esperaría normalmente. Nada sofisticado. Solo texto directo, fluido, con amor. Completamente neutral y sin elegir un lado o sentarse en la cerca. Simplemente es. Simplemente lo es. Le gusta donde está. No se siente obligado a elegir un bando. Déjalo ser. Será mejor así. Confía en mí.

## Alineación a la izquierda

Este es un párrafo. Está alineado a la izquierda. Debido a esto, es un poco más liberal en sus puntos de vista. Su color favorito es el verde. La alineación izquierda tiende a ser más ecológica, pero no proporciona evidencia concreta de que realmente lo sea. Aunque le gusta repartir la riqueza de forma equitativa, deja la distribución igualitaria a un alineamiento justificado.
{: .text-left}

## Alineación centrada

Este es un párrafo. Está alineado al centro. El centro es, pero la naturaleza, una niñera.  Tiene dificultades para decidirse. Quiere elegir un bando. Realmente, lo hace. Tiene las mejores intenciones, pero tiende a complicar las cosas más que a ayudar. Lo mejor que puedes hacer es tratar de ganártelo y esperar lo mejor. Escuché que Center align acepta sobornos.
{: .text-center}

## Right Aligned

Este es un párrafo. Está alineado a la derecha. Es un poco más conservador en sus puntos de vista. Prefiere que no le digan qué hacer o cómo hacerlo. Right align posee una gran cantidad de armas y le encanta ir al campo de tiro para practicar. Que mola y todo. Quiero decir, es un tiro bastante bueno desde al menos cuatro o cinco campos de fútbol de distancia. El jefe.
{: .text-right}

## Justify Aligned

Este es un párrafo. Se justifica alineado. Se enoja mucho cuando la gente lo asocia con Justin Timberlake. Por lo general, justificado es bastante sencillo. Le gusta que todo esté en su lugar y no todos los torcidos como el resto de los alineados. No digo que eso lo haga mejor que el resto de los alineados, pero tiende a posponer una actitud más elitista.
{: .text-justify}

## No Wrap

Este es un párrafo. No tiene envoltura. Costillas de ternera solomillo de cerdo filet mignon. Costillas de repuesto leberkas chuletón, burgdoggen fatback lomo biltong papada flanco solomillo hamburguesa tocino brisket. Paletilla bresaola baqueta capicola. Costillas de ternera prosciutto porchetta de ternera.
{: .text-nowrap}

---

Transforme texto con clases de mayúsculas a través de HTML:

```html
<p class="text-lowercase">Texto en minúsculas.</p>
<p class="text-uppercase">Texto en mayúsculas.</p>
<p class="text-capitalize">Texto en mayúscula.</p>
<p class="text-truncate">Texto tachado.</p>
<p class="small">Texto  pequeño.</p>
```

O mediante Kramdown:

```markdown
Texto en minúsculas
{: .text-lowercase}

Texto en mayúsuculas.
{: .text-uppercase}

Texto en mayúsucula.
{: .text-capitalize}
```

Texto en minúsculas.
{: .text-lowercase}

Texto en mayúsuculas.
{: .text-uppercase}

Texto en mayúscula.
{: .text-capitalize}

Este es un párrafo truncado de texto. Bacon ipsum dolor amet hombro papada cola andouille fatback lengua. Jamón porchetta kielbasa cerdo chuleta de cerdo, albóndiga de hamburguesa de lomo. Picanha porchetta porcina, pechuga salami panceta de cerdo burgdoggen. Pastrami de cerdo Cupim, mandril de tres puntas de cerdo, papada, pierna, alcatra, pechuga, capicola, punta de bola, prosciutto, costillas de res, doner. Chuleta de cerdo molida con tocino de tres puntas burgdoggen leberkas filete de lomo de cerdo carne de res en conserva salami.
{: .text-truncate}

Small text
{: .small}
