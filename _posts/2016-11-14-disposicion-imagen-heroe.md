---
title: "Disposición: Imagen de Heroe"
image: /assets/images/eder-oliveira-180877.jpg
categories:
  - Disposición
tags:
  - contenido
  - imagen
  - disposición
last_modified_at: 2017-03-17T10:46:49-04:00
---


Ésta publicación debe mostrar una imagen de héroe grande en la parte superior de una página.


Esta publicación prueba una imagen horizontal usando el siguiente material frontal de YAML:

```yaml
image: /assets/images/eder-oliveira-180877.jpg
```

Las imágenes heroicas también se pueden asignar de esta manera:

```yaml
image:
  path: /assets/images/eder-oliveira-180877.jpg
```
