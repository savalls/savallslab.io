---
title: "Markup: Alineación imagen"
categories:
  - Markup
tags:
  - alineación
  - subtítulos
  - contenido
  - css
  - imagen
  - markup
last_modified_at: 2017-03-09T11:15:57-05:00
---

La mejor manera de demostrar el flujo y reflujo de las diversas opciones de posicionamiento de imágenes es acomodarlas cómodamente entre un océano de palabras. Coge un remo y empecemos.

Asignar clases con HTML:

```html
<img src="image.jpg" class="align-left" alt="">
<img src="image.jpg" class="align-center" alt="">
<img src="image.jpg" class="align-right" alt="">
```

O mediante el uso de Kramdown y [listas de atributos en línea](https://kramdown.gettalong.org/syntax.html#inline-attribute-lists):

```markdown
![left-aligned-image](image.jpg){: .align-left}
![center-aligned-image](image.jpg){: .align-center}
![right-aligned-image](image.jpg){: .align-right}
```

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-580x300.jpg){: .align-center}

La imagen debe ser**centrada** mediante la clase `.align-center`.

![image-left]({{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-150x150.jpg){: .align-left} El resto de este párrafo es de relleno para ver el texto que se ajusta a la imagen de 150 × 150, que está **alineada a la izquierda** con la clase `.align-left`.

Como puede ver, debe haber un espacio arriba, abajo y a la derecha de la imagen. El texto no debe deslizarse sobre la imagen. Arrastrarse simplemente no está bien. Las imágenes también necesitan un respiro. Que hablen como tú palabras. Déjelos hacer su trabajo sin ningún problema con el texto. En aproximadamente una oración más aquí, veremos que el texto se mueve desde la derecha de la imagen hacia abajo debajo de la imagen en una transición perfecta. Una vez más, dejar que el hacer es cosa. ¡Misión cumplida!

Y ahora para una **imagen enormemente grande** --- no tiene **ninguna alineación**.

![sin-alineación]({{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-1200x4002.jpg)

La imagen de arriba, aunque `1200px` de ancho, no debe desbordar el área de contenido. Debe permanecer contenido sin interrupciones visibles en el flujo de contenido.

![imagen-derecha]({{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-300x200.jpg){: .align-right}

Y ahora vamos a cambiar las cosas a la **derecha** con la clase `.align-right`. Nuevamente, debe haber suficiente espacio arriba, abajo ya la izquierda de la imagen. Solo míralo allí --- ¡Oye chico! Manera de rockear ese lado derecho. No me importa lo que diga la imagen alineada a la izquierda, te ves genial. No dejes que nadie más te diga lo contrario.

En solo un poco aquí, debería ver que el texto comienza a ajustarse debajo de la imagen alineada a la derecha y se acomoda muy bien. Todavía debería haber mucho espacio y todo debería estar bien sentado. Sí --- Solo así. Nunca se sintió tan bien tener razón.

Y justo cuando pensabas que habíamos terminado, ¡los vamos a hacer de nuevo con subtítulos!

<figure class="align-center">
  <a href="#"><img src="{{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-580x300.jpg" alt=""></a>
  <figcaption>Mirar a 580 x 300 <a href="#">consiguiendo algo</a> amor.</figcaption>
</figure> 

La figura de arriba está **centrada**. El título también tiene un enlace, solo para ver si hace algo extraño.

<figure style="width: 150px" class="align-left">
  <img src="{{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-150x150.jpg" alt="">
  <figcaption>subtítulo minúsculo.</figcaption>
</figure> 

El resto de este párrafo es de relleno para ver el texto que se ajusta a la imagen de 150 × 150, que está **alineada a la izquierda** con la clase `.align-left`.

Como puede ver, debe haber un espacio arriba, abajo y a la derecha de la imagen. El texto no debe deslizarse sobre la imagen. Arrastrarse simplemente no está bien. Las imágenes también necesitan un respiro. Que hablen como tú palabras. Déjelos hacer su trabajo sin ningún problema con el texto. En aproximadamente una oración más aquí, veremos que el texto se mueve desde la derecha de la imagen hacia abajo debajo de la imagen en una transición perfecta. Una vez más, dejar que el hacer es cosa. ¡Misión cumplida!

Y ahora una imagen enormemente grande sin **sin alineación**.

<figure style="width: 1200px">
  <img src="{{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-1200x4002.jpg" alt="">
  <figcaption>Comentario de imagen masivo para tus globos oculares.</figcaption>
</figure> 

El elemento de la figura de arriba tiene un estilo en línea de `width: 1200px` establecido que debería permitir que se rompa fuera del flujo de contenido normal.

<figure style="width: 300px" class="align-right">
  <img src="{{ site.url }}{{ site.baseurl }}/assets/images/image-alignment-300x200.jpg" alt="">
  <figcaption>Se siente bien estar a la derecha.</figcaption>
</figure> 

Y ahora vamos a cambiar las cosas a la **derecha** con la clase `.align-right`. Nuevamente, debe haber suficiente espacio arriba, abajo ya la izquierda de la imagen. Solo míralo allí --- ¡Oye chico! Manera de rockear ese lado derecho. No me importa lo que diga la imagen alineada a la izquierda, te ves genial. No dejes que nadie más te diga lo contrario.

En solo un poco aquí, debería ver que el texto comienza a ajustarse debajo de la imagen alineada a la derecha y se acomoda muy bien. Todavía debería haber mucho espacio y todo debería estar bien sentado. Sí --- Solo así. Nunca se sintió tan bien tener razón.

¡Y eso es una envoltura, yo! Sobreviviste a las tumultuosas aguas del alineamiento. ¡Logro de alineación de imagen desbloqueado!